package org.isfce.pid.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import org.isfce.pid.dao.IUserJpaDao;
import org.isfce.pid.model.SecurityUser;
import org.isfce.pid.model.User;

/**
 * Retourne la liste des utilisateurs pour obtenir les "UserDetails" de
 * l'utilisateur connecté Nécessaire pour la configuration de la sécurité
 * 
 * @author Didier
 *
 */

@Service(value = "myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	private IUserJpaDao usersDAO;

	public MyUserDetailsService(IUserJpaDao usersDAO) {
		this.usersDAO = usersDAO;
	}
//MyUserDetailsService return a new SecurityUser
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> oUser = usersDAO.findById(username);
		List<GrantedAuthority> authorities = new ArrayList<>();
		if (oUser.isPresent()){
			authorities.add(new SimpleGrantedAuthority(oUser.get().getRole().name()));
			return new org.springframework.security.core.userdetails.User(oUser.get().getUsername(), oUser.get().getPassword(), authorities);
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

}
