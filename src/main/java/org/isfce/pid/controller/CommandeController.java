package org.isfce.pid.controller;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.isfce.pid.controller.dto.ProduitDto;
import org.isfce.pid.controller.wrapper.CommandeWrapper;
import org.isfce.pid.model.Commande;
import org.isfce.pid.model.LigneCmd;
import org.isfce.pid.model.LigneCmdPkId;
import org.isfce.pid.model.Produit;
import org.isfce.pid.model.Site;
import org.isfce.pid.service.CategorieServices;
import org.isfce.pid.service.CommandeServices;
import org.isfce.pid.service.LigneCmdServices;
import org.isfce.pid.service.ProduitServices;
import org.isfce.pid.service.SiteProductCodeServices;
import org.isfce.pid.service.SiteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/site")
public class CommandeController {

	@Autowired
	private SiteServices siteServices;

	@Autowired
	private ProduitServices produitServices;

	@Autowired
	private CategorieServices categorieServices;

	@Autowired
	private CommandeServices commandeServices;

	@Autowired
	private SiteProductCodeServices siteProductCodeServices;

	@Autowired
	private LigneCmdServices ligneCmdServices;
//    public OrderController(OrderSelectionService orderSelectionService) {
//        this.orderSelectionService = orderSelectionService;
//    }

	@GetMapping("/liste")
	public String showSites(Model model) {
		List<Site> sites = siteServices.getAllSites();
		model.addAttribute("sites", sites);
		return "site/listeSites";
	}

	@GetMapping("/create-new-order")
	public String submitcommande(@RequestParam(value = "searchTerm", required = false) String searchTerm, Model model) {
		// 1.Initializing a Wrapper:
		CommandeWrapper commandeWrapper = new CommandeWrapper();

		// 2.Retrieving Data, retrieves all sites, produits objects from the database

		// Charger la liste des sites disponibles
		List<Site> sites = siteServices.getAllSites();
		model.addAttribute("sites", sites);

		List<Produit> produits;

		if (searchTerm != null && !searchTerm.isEmpty()) {
			Produit produit = produitServices.findByNomProduitIgnoreCase(searchTerm);

			if (produit != null) {

				produits = Collections.singletonList(produit);

			} else {
				produits = Collections.emptyList();
			}
		} else {
			produits = produitServices.getAllProduits();
		}

		// Convert the list of Produit objects to ProduitDto objects
		List<ProduitDto> produitsDto = produits.stream().map(Produit::toDto).collect(Collectors.toList());

		// The list of ProduitDto objects is then added to a ProduitDtoWrapper object,
		// which is used to pass the data to the view.
		commandeWrapper.setProduitsDto(produitsDto);

		model.addAttribute("commandeWrapper", commandeWrapper);

		return "commande/commandeProduitsForm";
	}

	@PostMapping("/submit-new-order")
	public String submitcommande(@ModelAttribute("commandeWrapper") @Valid CommandeWrapper commandeWrapper,
			BindingResult errors, Model model) {
		Produit produit; // Declare it here
		// Retrieve the selected site
		Site selectedSite = commandeWrapper.getSite();
		model.addAttribute("selectedSite", selectedSite);
		commandeWrapper.setSite(selectedSite);
		// boolean isOrdinaire =
		// commandeWrapper.getProduitsDto().get(0).getIsOrdinaire();
		boolean isOrdinaire = commandeWrapper.getProduitsDto().get(0).getOrdinaire();

		System.out.println("Ordinaire is " + isOrdinaire);
		// Create a new Commande
		Commande commande = new Commande();
		commande.setOrderDate(LocalDate.now());
		// Set the typeBudget based on isOrdinaire
		commande.setTypeBudget(isOrdinaire ? "ordinaire" : "extraordinaire");
		commande.setSite(commandeWrapper.getSite());
		// Save the Commande to the database
		commande = commandeServices.insert(commande);

		// Filter selected products...I get only selected ProduitDtos on the web page
		List<ProduitDto> produitDtos = commandeWrapper.getProduitsDto().stream().filter(ProduitDto::isSelected)
				.collect(Collectors.toList());

		model.addAttribute("produitsDto", produitDtos);

		// Print the contents of the produitsDto list
		for (ProduitDto produitDto : produitDtos) {
			System.out.println("ID: " + produitDto.getId());
			System.out.println("Nom Produit: " + produitDto.getNomProduit());
			System.out.println("Prix: " + produitDto.getPrix());
			System.out.println("Selected: " + produitDto.isSelected());
			System.out.println("Quantity: " + produitDto.getQuantity());
			System.out.println("--------------------------------");
		}
		for (ProduitDto produitDto : produitDtos) {
			produit = produitDto.toProduit(categorieServices);
			System.out.println("Debug: produit = " + produit);
			System.out.println("Name: " + produit.getNomProduit());

			produit = produitServices.findOne(produitDto.getId());
			int orderedQuantity = produitDto.getQuantity();
			int currentStock = produit.getStock();
			produit.setStock(currentStock - orderedQuantity);
			// Save the updated product back to the database
			System.out.println("the currentStock is: " + currentStock);
			//Save the updated product back to the database
			produitServices.update(produit);

		}

		log.debug("Recherche des produitsDto");

		LocalDate orderDate = LocalDate.now();
		// commandeWrapper.setProduitsDto(selectedProduitDtos);
		// Outputting the instance's values

		for (ProduitDto produitDto : produitDtos) {
			produit = produitDto.toProduit(categorieServices);
			System.out.println("Name: " + produit.getNomProduit());
			System.out.println("Price: " + produitDto.getPrix());
			System.out.println("Quantity: " + produitDto.getQuantity());
			System.out.println("------------");
		}

		// Retrieve the code based on selected site and product
		Set<LigneCmd> lignesCmd = new HashSet<>();
		for (ProduitDto produitDto : produitDtos) {
			produit = produitDto.toProduit(categorieServices);
			Optional<String> optionalCode = siteProductCodeServices.getCodeBySiteAndProduct(selectedSite.getId(),
					produit.getId());
			if (optionalCode.isPresent()) {
				String code = optionalCode.get();
				LigneCmdPkId ligneCmdPkId = new LigneCmdPkId();
				ligneCmdPkId.setCommande(commande.getId()); // Set the ID of the associated Commande
				ligneCmdPkId.setProduit(produit.getId()); // Set the ID of the associated Produit

				LigneCmd ligneCmd = new LigneCmd();
				ligneCmd.setId(ligneCmdPkId);
				ligneCmd.setCommande(commande);
				ligneCmd.setProduit(produit);
				ligneCmd.setQuantite(produitDto.getQuantity());
				ligneCmd.setPrix(produit.getPrix() * produitDto.getQuantity());
				ligneCmd.setCode(code); // Set the code to WhatType

				lignesCmd.add(ligneCmd);
			}
		}
		ligneCmdServices.insert(lignesCmd);
		commande.setSite(selectedSite);
		commande.setLignesCmd(lignesCmd);

		float total = commande.calculateTotal();

		System.out.println(commandeWrapper);

		model.addAttribute("totalPrice", total);
		model.addAttribute("commandeWrapper", commandeWrapper);
		// Pass only the selected ProduitDto objects to the view
		model.addAttribute("produitsDto", produitDtos);
		return "commande/success";
	}
}
