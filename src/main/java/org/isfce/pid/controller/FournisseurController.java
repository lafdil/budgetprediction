package org.isfce.pid.controller;

import java.util.List;

import javax.validation.Valid;

import org.isfce.pid.controller.dto.FournisseurDto;
import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.model.Fournisseur;
import org.isfce.pid.service.FournisseurServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/fournisseur")
public class FournisseurController {
	
	@Autowired
	FournisseurServices fournisseurServices;
	
	
	@GetMapping("/liste")
	public String showSites(Model model) {
		List<Fournisseur> fournisseurs = fournisseurServices.getAllFournisseurs();
		model.addAttribute("fournisseurs", fournisseurs);
		log.info("liste des Fournisseurs");
		return "fournisseur/listeFournisseurs";
	}

	@GetMapping("/{id}")
	public String detailFournisseur(@PathVariable int id, Model model) {
		log.debug("Recherche le Fournisseur: " + id);
		// si ce n'est pas une redirection, charge le fournisseur
		if (!model.containsAttribute("fournisseur"))
			// recherche le fournisseur dans la liste et ajout au model
			model.addAttribute("fournisseur", (fournisseurServices.findOne(id).toDto()));
		return "fournisseur/fournisseur";
	}
	
	
	
	
	
	@GetMapping("/{id}/update")
	public String updateFournisseurGet(@PathVariable(name = "id") int id, Model model) {
	
		FournisseurDto fournisseurDto = (fournisseurServices.findOne(id).toDto());
		model.addAttribute("fournisseur", fournisseurDto);
	
		System.out.println(fournisseurDto);
		log.debug("affiche la vue pour modifier un site ");
		return "fournisseur/updateFournisseur";
	}
	
	@PostMapping("{fournisseur}/update")
	public String updateFournisseurPost(@PathVariable(name = "fournisseur") int idFournisseur,
			@ModelAttribute("fournisseur") @Valid FournisseurDto fournisseurDto, BindingResult errors, Model model,
			RedirectAttributes rModel) {
		log.info("POST update d'un ingredient id: " + idFournisseur + " en " + fournisseurDto);


		if (errors.hasErrors()) {
			// rajoute id ingredient pour savoir le code initial de l'ingredient
			model.addAttribute("savedId", idFournisseur);
			log.info("Erreurs dans les données du fournisseur:" + fournisseurDto.getId());
			return "fournisseur/updateFournisseur";
		}

		if (fournisseurDto.getId() == idFournisseur) {
			fournisseurServices.update(fournisseurDto.toFournisseur());
		}

		else
			if (fournisseurServices.existsById(fournisseurDto.getId())){
			// rajoute id	Fournisseur pour savoir l'id initial du fournisseur
			model.addAttribute("savedId", idFournisseur);
			errors.rejectValue("id", "fournisseur.id.doublon", "DUPLICATE ERROR");
			return "fournisseur/updateFournisseur";
		} else { // supprime l'objet avec l'ancien code et rajoute le nouveau
			fournisseurServices.delete(idFournisseur);
			fournisseurServices.insert(fournisseurDto.toFournisseur());
		}

		// Préparation des attributs Flash pour survivre à  la redirection
		// Ainsi dans l'affichage du détail de l'ingredient on ne devra pas chercher
		// l'ingredient dans la BD
		rModel.addFlashAttribute("fournisseur", fournisseurDto);
		// hihihi
		log.debug("redirection détail du Fournisseur");
		return "redirect:/fournisseur/" + fournisseurDto.getId();
	}

	/**
	 * Supression d'un Fournisseur
	 * 
	 * @param id de Fournisseur
	 * @return le mapping de redirection
	 */
	@PostMapping("/{id}/delete")
	public String deleteFournisseur(@PathVariable int id) {
		if (fournisseurServices.existsById(id)) {
			fournisseurServices.delete(id);
			System.out.println("Hi delete");
		} else
			throw new NotExistException(id);
		log.debug("Suppression réussie de Fournisseur: " + id);
		return "redirect:/fournisseur/liste";

	}
	

	@GetMapping("/add")
	public String addFournisseurGet(@ModelAttribute("fournisseur") FournisseurDto fournisseur) {
		log.debug("affiche la vue pour ajouter un Fournisseur ");

				fournisseur.setId(fournisseurServices.getNumberOfFournisseurs() + 1);

		return "fournisseur/addFournisseur";
	}
	
	@PostMapping("/add")
	public String addFournisseurPost(@Valid @ModelAttribute("fournisseur") FournisseurDto fournisseurDto, BindingResult errors,
			RedirectAttributes rModel) {

		log.info("POST d'un fournisseur" + fournisseurDto);
		// Step 1: Validation
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du fournisseur:" + fournisseurDto.getId());
			// If there are errors, show the form again
			return "fournisseur/addFournisseur";
		}

	
		if (fournisseurServices.existsById(fournisseurDto.getId())) {
			errors.rejectValue("id", "fournisseur.code.doublon", "DUPLICATE ERROR");
			return "fournisseur/addFournisseur"; // If duplicate, show the form again with an error message
		}
		// Step 3: Save the course if valid and not duplicate

		// Step 3: Save the fournisseur if valid and not duplicate
		Fournisseur fournisseur = fournisseurDto.toFournisseur();
		fournisseurServices.insert(fournisseur);

		// Step 4: Prepare attributes to survive redirection
		rModel.addFlashAttribute("fournisseur", fournisseurDto);

		log.debug("redirection  to fournisseur details page");

		return "redirect:/fournisseur/" + fournisseurDto.getId();
	}
	
}

