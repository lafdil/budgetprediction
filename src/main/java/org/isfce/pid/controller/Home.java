package org.isfce.pid.controller;

import org.isfce.pid.model.Roles;
import org.isfce.pid.model.User;
import org.isfce.pid.service.UserServices;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class Home {
	@Value("${spring.thymeleaf.encoding}")
	String thyme1;

	@GetMapping("/")
	public String home(Model model) {
		model.addAttribute("prof", "VO");
		model.addAttribute("cours", "PID");
		model.addAttribute("thyme", thyme1);
		return "home";
	}

}


