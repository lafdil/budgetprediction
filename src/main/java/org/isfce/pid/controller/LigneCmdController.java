package org.isfce.pid.controller;

import java.util.List;

import org.isfce.pid.dao.ILigneCmdJpaDao;
import org.isfce.pid.model.LigneCmd;
import org.isfce.pid.model.LigneCmdPkId;
import org.isfce.pid.service.LigneCmdServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j

@Controller
public class LigneCmdController {

	@Autowired
	private ILigneCmdJpaDao iLigneCmdJpaDao;
	
	@Autowired
	private LigneCmdServices ligneCmdServices;

	@GetMapping("/ligneCmd")
	public String getAllLigneCmd(Model model) {
		List<LigneCmd> lignes = iLigneCmdJpaDao.findAll();
		model.addAttribute("lignes", lignes);
		return "ligneCmd/listeLigneCmd"; // This should be the name of your HTML template
	}
	
	@GetMapping("/ligneCmd/{commandeId}/{produitId}")
	public String detailLigneCmd(@PathVariable int commandeId, @PathVariable int produitId, Model model) {
	    log.debug("Recherche la ligne de commande avec Commande ID: " + commandeId + " et Produit ID: " + produitId);
	    
	    // Build LigneCmdPkId
	    LigneCmdPkId id = new LigneCmdPkId(commandeId, produitId);
	    
	    // Load the LigneCmd entity
	    LigneCmd ligneCmd = ligneCmdServices.findOne(commandeId, produitId);	    
	    if (ligneCmd != null) {
	        model.addAttribute("ligneCmd", ligneCmd);
	    }
	    
	    return "ligneCmd/ligneCmdDetail";
	}

	
	
}
