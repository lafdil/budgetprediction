package org.isfce.pid.controller;

import javax.validation.Valid;

import org.isfce.pid.controller.dto.ProduitDto;
import org.isfce.pid.model.Produit;
import org.isfce.pid.service.CategorieServices;
import org.isfce.pid.service.ProduitServices;
import org.isfce.pid.controller.exceptions.NotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/produit")
public class ProduitController {

	@Autowired
	ProduitServices produitServices;

	@Autowired
	private CategorieServices categorieServices;

	/**
	 * Liste des Produits
	 * 
	 * @param model
	 * @return vue
	 */

	@GetMapping("/liste")
	public String listeProduits(@RequestParam(value = "searchTerm", required = false) String searchTerm, Model model) {
		List<Produit> produitsList;

		if (searchTerm != null && !searchTerm.isEmpty()) {
			Produit produit = produitServices.findByNomProduitIgnoreCase(searchTerm);

			produitsList = new ArrayList<>();

			if (produit != null) {
				produitsList.add(produit);

			}
		} else {

			produitsList = produitServices.findAll();
		}

		model.addAttribute("produitsList", produitsList);
		model.addAttribute("searchTerm", searchTerm);

		log.info("liste des Produits");

		return "produit/listeProduits";
	}


	@GetMapping("/add")
	public String addProduitGet(@ModelAttribute("produit") ProduitDto produit) {
		log.debug("affiche la vue pour ajouter un produit ");

		produit.setId(produitServices.getNumberOfProduits() + 1);

		return "produit/addProduit";
	}

	@PostMapping("/add")
	public String addProduitPost(@Valid @ModelAttribute("produit") ProduitDto produitDto, BindingResult errors,
			RedirectAttributes rModel) {

		log.info("POST d'un produit" + produitDto);
		// Step 1: Validation
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du produit:" + produitDto.getId());
			// If there are errors, show the form again
			return "produit/addProduit";
		}

		/*
		 * In summary, Step 2 checks if a course with the given code already exists in
		 * the system. If it does, it adds a validation error to indicate a duplicate
		 * entry, and then it returns to the form page, allowing the user to correct the
		 * input and try again.
		 */

		// Step 2: Check if produit already exists
		if (produitServices.existsById(produitDto.getId())) {
			errors.rejectValue("id", "produit.code.doublon", "DUPLICATE ERROR");
			return "produit/addProduit"; // If duplicate, show the form again with an error message
		}
		// Step 3: Save the course if valid and not duplicate

		// Step 3: Save the product if valid and not duplicate
		Produit produit = produitDto.toProduit(categorieServices);
		produitServices.insert(produit);

		// Step 4: Prepare attributes to survive redirection
		rModel.addFlashAttribute("produit", produitDto);

		log.debug("redirection  to produit details page");

		return "redirect:/produit/" + produitDto.getId();
	}

	/**
	 * Affiche le détail d'un cours
	 * 
	 * @param code
	 * @param model
	 * @param locale
	 * @return
	 */
	@GetMapping("/{id}")
	public String detailProduit(@PathVariable int id, Model model) {
		log.debug("Recherche le produit: " + id);
		// si ce n'est pas une redirection, charge le produit
		if (!model.containsAttribute("produit"))
			// recherche le produit dans la liste et ajout au model
			model.addAttribute("produit", (produitServices.findOne(id).toDto()));
		return "produit/produit";
	}

	@GetMapping("/{produitId}/update")
	public String updateProduitGet(@PathVariable(name = "produitId") int id, Model model) {
		// rajoute l'produitDTO ou déclenche une exception
		ProduitDto produitDto = (produitServices.findOne(id).toDto());
		model.addAttribute("produit", produitDto);
		model.addAttribute("savedId", id);
		// rajoute l'id du cours pour gérer la maj de ce dernier (problème de maj de la
		// PK)
		System.out.println(produitDto);
		log.debug("affiche la vue pour modifier un produit ");
		return "produit/updateProduit";
	}

	@PostMapping("{produit}/update")
	public String updateProduitPost(@PathVariable(name = "produit") int idProduit,
			@ModelAttribute("produit") @Valid ProduitDto produitDto, BindingResult errors, Model model,
			RedirectAttributes rModel) {
		log.info("POST update d'un produit id: " + idProduit + " en " + produitDto);

		// means that the produit with this ID does not exist,
//		if (idproduit == 0)
//			throw new NotExistException(0);
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			// rajoute id produit pour savoir le code initial de l'produit
			model.addAttribute("savedId", idProduit);
			log.info("Erreurs dans les données du Produit:" + produitDto.getId());
			return "produit/updateProduit";
		}
		Produit prod;

		if (produitDto.getId() == idProduit) {
//			prod = produitDto.toProduit(categorieServices); // Convert ProduitDto to Produit
//			produitServices.update(prod);
			
			produitServices.update(produitDto.toProduit(categorieServices));
		}

		else
		/*
		 * the exists() method of the produitServices object is called with the id
		 * field of the produitDto object. If the method returns true, a validation
		 * error message is added to the errors object
		 */
		if (produitServices.existsById(produitDto.getId())) {
			// rajoute idproduit pour savoir l'id initial de l'produit
			model.addAttribute("savedId", idProduit);
			errors.rejectValue("id", "produit.id.doublon", "DUPLICATE ERROR");
			return "produit/updateProduit";
		} else { // supprime l'objet avec l'ancien code et rajoute le nouveau
			produitServices.delete(idProduit);
			produitServices.insert(produitDto.toProduit(categorieServices));
		}

		// Préparation des attributs Flash pour survivre à  la redirection
		// Ainsi dans l'affichage du détail de l'produit on ne devra pas chercher
		// l'produit dans la BD
		rModel.addFlashAttribute("produit", produitDto);
		// hihihi
		log.debug("redirection détail du produit");
		return "redirect:/produit/" + produitDto.getId();
	}

	/**
	 * Supression d'un Produit
	 * 
	 * @param id de Produit
	 * @return le mapping de redirection
	 */
	@PostMapping("/{id}/delete")
	public String deleteProduit(@PathVariable int id) {
		if (produitServices.existsById(id)) {
			produitServices.delete(id);
			System.out.println("Hi delete");
		} else
			throw new NotExistException(id);
		log.debug("Suppression réussie de produit: " + id);
		return "redirect:/produit/liste";

	}

}

