package org.isfce.pid.controller.dto;

import java.util.HashSet;
import java.util.Set;

import org.isfce.pid.model.Produit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommandeDto {

	private int id;
	private String siteName;
	protected Set<Produit> produits = new HashSet<Produit>();

	// Just for the binding, are not used in the constructor because we already know
	// their values
	private boolean selected;
	public int quantity;
	

}
