package org.isfce.pid.controller.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.isfce.pid.model.Fournisseur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FournisseurDto {

    private Integer id;

    @NotNull
    private String nomFournisseur;

    

    public Fournisseur toFournisseur() {
        Fournisseur fournisseur = new Fournisseur();
        fournisseur.setId(this.id);
        fournisseur.setNomFournisseur(this.nomFournisseur);
     
        return fournisseur;
    }

   
}
