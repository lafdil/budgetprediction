package org.isfce.pid.controller.dto;

import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.isfce.pid.model.Produit;
import org.isfce.pid.service.CategorieServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.isfce.pid.model.Categorie;
import org.isfce.pid.model.Fournisseur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProduitDto {

	@Autowired
	private CategorieServices categorieServices;

	public ProduitDto(CategorieServices categorieServices) {
		this.categorieServices = categorieServices;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "NOM_PRODUIT", length = 40, nullable = false)
	private String nomProduit;

    private Categorie categorie;


	@NotNull
	@Column(name = "PRIX_PAR_PIECE", nullable = false)
	private double prix;

	@NotNull
	private int stock;

	private String description;
	
	private boolean selected;
	
	public int quantity;

	private boolean isOrdinaire;
	
	    public boolean getOrdinaire() {
        return isOrdinaire;
    }

    public void setOrdinaire(boolean ordinaire) {
        isOrdinaire = ordinaire;
    }
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
    public Produit toProduit(CategorieServices categorieServices) {
        Categorie categorie = this.categorie; // Use the existing categorie attribute
        
        // If needed, you can also fetch the Categorie from the service using:
        // Categorie categorie = categorieServices.fetchCategorieById(this.categorie).orElse(null);
        
        Produit produit = new Produit(id, nomProduit, categorie, prix, stock, description, isOrdinaire);
   
        
        return produit;
    }



    



}