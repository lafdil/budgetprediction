package org.isfce.pid.controller.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.isfce.pid.model.Budget;
import org.isfce.pid.model.Commande;
import org.isfce.pid.model.Site;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SiteDto {

	private Integer id;
	
	private String nomSite;
	
	private String nomDemandeur;
	private String adresse;

	   public Site toSite() {
	        Site site = new Site();
	        site.setId(this.id);
	        site.setNomSite(this.nomSite);
	        site.setAdresse(this.adresse);
	        site.setNomDemandeur(this.nomDemandeur);
	        return site;
	    }

}
