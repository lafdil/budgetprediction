package org.isfce.pid.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.isfce.pid.controller.dto.ProduitDto;
import org.isfce.pid.controller.dto.SiteDto;
import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.model.Commande;
import org.isfce.pid.model.Produit;
import org.isfce.pid.model.ReportForm;
import org.isfce.pid.model.Site;
import org.isfce.pid.service.SiteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class siteController {

	@Autowired
	private SiteServices siteServices;

	@GetMapping("/commandes/{siteId}")
	public String getCommandesWithTotalPrice(@PathVariable Integer siteId, Model model) {

		try {
			List<Object[]> commandesWithTotalPrice = siteServices.findCommandesWithTotalPrice(siteId);
			Site OrdersSite = siteServices.getSiteById(siteId);

			/*
			 * for (Object[] objectArray : commandesWithTotalPrice) { Commande commande =
			 * (Commande) objectArray[0]; // Assuming objectArray[0] is a Commande
			 * System.out.println(commande); Integer orderId = commande.getId(); // Get the
			 * order ID from the Commande object System.out.println(orderId);
			 * 
			 * Double totalPrice = (Double) objectArray[1]; System.out.println(totalPrice);
			 * }
			 */
// example [Commande{id=38, orderDateTime=2023-09-08T12:40:55.537149, site=2}, 5850.0]
			for (Object[] objectArray : commandesWithTotalPrice) {
				if (objectArray[0] instanceof Commande) {
					// Commande{id=38, orderDateTime=2023-09-08T12:40:55.537149, site=2}
					Commande commande = (Commande) objectArray[0];

					System.out.println(commande);
				}
			}
			model.addAttribute("OrdersSite", OrdersSite);

			model.addAttribute("commandesWithTotalPrice", commandesWithTotalPrice);
			return "site/commandesWithTotalPriceView";
		} catch (Exception e) {
			log.error("Error fetching commandes with total price", e);
			// Handle the exception or log it for further investigation
			return "errorPage"; // Provide a suitable error page
		}
	}

	@GetMapping("/site/report")
	public String generateCommandesReport(Model model) {
		List<Site> sites = siteServices.getAllSites();
		model.addAttribute("sites", sites);
		model.addAttribute("reportForm", new ReportForm()); // Add a new ReportForm to the model

		return "site/reportTemplate";
	}

	@PostMapping("/commandes/site/year")
	public String postCommandesBySiteAndYear(@ModelAttribute ReportForm reportForm, Model model) {
		try {
			Integer siteId = reportForm.getSiteId();
			Integer year = reportForm.getYear(); // Get the selected year from the form
			List<Object[]> commandesWithTotalPriceAndDateRange = siteServices.findCommandesBySiteAndYear(siteId, year);

			double totalOrdinaryPrices = 0.0;
			double totalExtraOrdinaryPrices = 0.0;
			double totalPrices = 0.0;

			for (Object[] result : commandesWithTotalPriceAndDateRange) {
				Commande commande = (Commande) result[0];
				double totalPrice = (double) result[1];

				if ("ordinaire".equals(commande.getTypeBudget())) {
					totalOrdinaryPrices += totalPrice;
				} else if ("extraordinaire".equals(commande.getTypeBudget())) {
					totalExtraOrdinaryPrices += totalPrice;
				}
			}
			totalPrices = totalOrdinaryPrices + totalExtraOrdinaryPrices;
			model.addAttribute("commandesWithTotalPriceAndDateRange", commandesWithTotalPriceAndDateRange);
			model.addAttribute("totalOrdinaryPrices", totalOrdinaryPrices);
			model.addAttribute("totalExtraOrdinaryPrices", totalExtraOrdinaryPrices);
			model.addAttribute("totalPrices", totalPrices);
			model.addAttribute("siteId", siteId);

			return "site/commandesWithTotalPriceAndDateRangeView";

		} catch (Exception e) {
			log.error("Error fetching commandes with total price", e);
			return "errorPage";
		}
	}

	@GetMapping("/site/{id}")
	public String detailSite(@PathVariable int id, Model model) {
		log.debug("Recherche le Site: " + id);
		// si ce n'est pas une redirection, charge le site
		if (!model.containsAttribute("site"))
			// recherche le site dans la liste et ajout au model
			model.addAttribute("site", (siteServices.findOne(id).toDto()));
		return "site/site";
	}

	@GetMapping("/site/{id}/update")
	public String updateSiteGet(@PathVariable(name = "id") int id, Model model) {
		// rajoute l'ingredientDTO ou déclenche une exception
		SiteDto siteDto = (siteServices.findOne(id).toDto());
		model.addAttribute("site", siteDto);
		// rajoute l'id du cours pour gérer la maj de ce dernier (problème de maj de la
		// PK)
		System.out.println(siteDto);
		log.debug("affiche la vue pour modifier un site ");
		return "site/updateSite";
	}

	@PostMapping("/site/{site}/update")
	public String updateSitePost(@PathVariable(name = "site") int idSite,
			@ModelAttribute("site") @Valid SiteDto siteDto, BindingResult errors, Model model,
			RedirectAttributes rModel) {
		log.info("POST update d'un ingredient id: " + idSite + " en " + idSite);

		// means that the ingredient with this ID does not exist,
//		if (idIngredient == 0)
//			throw new NotExistException(0);
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			// rajoute id ingredient pour savoir le code initial de l'ingredient
			model.addAttribute("savedId", idSite);
			log.info("Erreurs dans les données du Produit:" + siteDto.getId());
			return "produit/updateProduit";
		}
		Site site;

		if (siteDto.getId() == idSite) {
//			prod = produitDto.toProduit(categorieServices); // Convert ProduitDto to Produit
//			produitServices.update(prod);

			siteServices.update(siteDto.toSite());
		}

		else
		/*
		 * the exists() method of the ingredientServices object is called with the id
		 * field of the IngredientDto object. If the method returns true, a validation
		 * error message is added to the errors object
		 */
		if (siteServices.existsById(siteDto.getId())) {
			// rajoute idIngredient pour savoir l'id initial de l'ingredient
			model.addAttribute("savedId", idSite);
			errors.rejectValue("id", "produit.id.doublon", "DUPLICATE ERROR");
			return "ingredient/updateProduit";
		} else { // supprime l'objet avec l'ancien code et rajoute le nouveau
			siteServices.delete(idSite);
			siteServices.insert(siteDto.toSite());
		}

		// Préparation des attributs Flash pour survivre à  la redirection
		// Ainsi dans l'affichage du détail de l'ingredient on ne devra pas chercher
		// l'ingredient dans la BD
		rModel.addFlashAttribute("produit", siteDto);
		// hihihi
		log.debug("redirection détail du produit");
		return "redirect:/site/" + siteDto.getId();
	}

	@GetMapping("/site/add")
	public String addSiteGet(@ModelAttribute("site") SiteDto site) {
		log.debug("affiche la vue pour ajouter un site ");

		site.setId(siteServices.getNumberOfSites() + 1);

		return "site/addSite";
	}

	@PostMapping("/site/add")
	public String addSitePost(@Valid @ModelAttribute("site") SiteDto siteDto, BindingResult errors,
			RedirectAttributes rModel) {

		log.info("POST d'un site" + siteDto);
		// Step 1: Validation
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du site:" + siteDto.getId());
			// If there are errors, show the form again
			return "site/addSite";
		}

		if (siteServices.existsById(siteDto.getId())) {
			errors.rejectValue("id", "site.code.doublon", "DUPLICATE ERROR");
			return "site/addSite"; // If duplicate, show the form again with an error message
		}
		// Step 3: Save the course if valid and not duplicate

		// Step 3: Save the site if valid and not duplicate
		Site site = siteDto.toSite();
		siteServices.insert(site);

		// Step 4: Prepare attributes to survive redirection
		rModel.addFlashAttribute("site", siteDto);

		log.debug("redirection  to site details page");

		return "redirect:/site/" + siteDto.getId();
	}

	/**
	 * Supression d'un Produit
	 * 
	 * @param id de Produit
	 * @return le mapping de redirection
	 */
	@PostMapping("/site/{id}/delete")
	public String deleteSite(@PathVariable int id) {
		if (siteServices.existsById(id)) {
			siteServices.delete(id);
		} else
			throw new NotExistException(id);
		log.debug("Suppression réussie du site: " + id);
		return "redirect:/site/liste";

	}

}
