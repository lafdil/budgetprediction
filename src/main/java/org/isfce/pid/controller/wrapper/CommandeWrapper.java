package org.isfce.pid.controller.wrapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.isfce.pid.controller.dto.ProduitDto;
import org.isfce.pid.controller.dto.SiteDto;
import org.isfce.pid.model.Commande;
import org.isfce.pid.model.Site;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommandeWrapper {

	private List<ProduitDto> produitsDto = new ArrayList<>(); // Initialize with an empty list

	// for later private SiteDto siteDto;

	private Site site; // Use a single Site object

	public void setProduitsDto(List<ProduitDto> produitsDto) {
		this.produitsDto = produitsDto;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public List<ProduitDto> getProduitsDto() {
		return produitsDto;
	}

	public Site getSite() {
		return site;
	}

	@Override
	public String toString() {
		return "CommandeWrapper{" + "produitsDto=" + produitsDto + ", site=" + site + '}';
	}

}
