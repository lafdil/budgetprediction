package org.isfce.pid.dao;

import org.isfce.pid.model.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICategorieJpaDao extends JpaRepository <Categorie, Integer>{
	
	// Other method declarations can be included here if needed
	//Categorie findByNomCategorieIgnoreCase(String searchTerm);

	Categorie findById(int id);
}
