package org.isfce.pid.dao;

import org.isfce.pid.model.Commande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface ICommandeJpaDao extends JpaRepository <Commande, Integer> {
	
	
	  @Query("SELECT SUM(l.prix) FROM TCOMMANDE c JOIN c.lignesCmd l WHERE c.id = :commandeId"
	  ) Float calculateTotalPrice(@Param("commandeId") Integer commandeId);
	 


}
