package org.isfce.pid.dao;

import org.isfce.pid.model.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFournisseurJpaDao extends JpaRepository <Fournisseur, Integer>  {

}
