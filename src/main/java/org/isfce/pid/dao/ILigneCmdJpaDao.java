package org.isfce.pid.dao;

import org.isfce.pid.model.LigneCmd;
import org.isfce.pid.model.LigneCmdPkId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILigneCmdJpaDao extends JpaRepository<LigneCmd,LigneCmdPkId> {

}
