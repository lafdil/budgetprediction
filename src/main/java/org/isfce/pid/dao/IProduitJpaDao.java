package org.isfce.pid.dao;

import org.isfce.pid.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProduitJpaDao extends JpaRepository<Produit, Integer>{

	boolean existsById(int id);
		
	// Other method declarations can be included here if needed
	Produit findByNomProduitIgnoreCase(String searchTerm);

}
