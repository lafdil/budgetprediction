package org.isfce.pid.dao;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.mapping.Map;
import org.isfce.pid.model.Commande;
import org.isfce.pid.model.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISiteJpaDao extends JpaRepository<Site, Integer> {

	@Query("SELECT c, SUM(l.prix) FROM TSITE s JOIN s.commands c JOIN c.lignesCmd l WHERE s.id = :siteId GROUP BY c.id")
	List<Object[]> findCommandesWithTotalPrice(@Param("siteId") Integer siteId);

	
//	@Query("SELECT c, SUM(l.prix) FROM TCOMMANDE c JOIN c.lignesCmd l WHERE c.site.id = :siteId AND c.orderDate >= :startDate AND c.orderDate <= :endDate GROUP BY c.id")
//	List<Object[]> findCommandesBySiteAndDateRange(@Param("siteId") Integer siteId,
//	                                              @Param("startDate") LocalDate startDate,
//	                                              @Param("endDate") LocalDate endDate);

	@Query("SELECT c, SUM(l.prix) FROM TCOMMANDE c JOIN c.lignesCmd l WHERE c.site.id = :siteId AND YEAR(c.orderDate) = :year - 1 GROUP BY c.id")
	List<Object[]> findCommandesBySiteAndYear(@Param("siteId") Integer siteId, @Param("year") Integer year);

}