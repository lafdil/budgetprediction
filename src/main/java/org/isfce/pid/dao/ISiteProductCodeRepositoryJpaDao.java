package org.isfce.pid.dao;

import java.util.Optional;

import org.isfce.pid.model.Site;
import org.isfce.pid.model.SiteProductCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ISiteProductCodeRepositoryJpaDao extends JpaRepository<SiteProductCode, Integer> {

	@Query("SELECT spc.code FROM SiteProductCode spc WHERE spc.site.id = :siteId AND spc.produit.id = :productId")
    Optional<String> findCodeBySiteAndProduct(@Param("siteId") int siteId, @Param("productId") int productId);

}
