package org.isfce.pid.dataloaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.isfce.pid.dao.IFournisseurJpaDao;
import org.isfce.pid.model.Fournisseur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class DataLoaderSuppliers implements ApplicationRunner {
    
    @Autowired
    private IFournisseurJpaDao iFournisseurJpaDao;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Load CSV data and insert into Fournisseur table
        try {
            ClassPathResource resource = new ClassPathResource("suppliers.csv");
            InputStream inputStream = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                Fournisseur fournisseur = new Fournisseur();
                fournisseur.setId(Integer.parseInt(values[0]));
                fournisseur.setNomFournisseur(values[1]);
                iFournisseurJpaDao.save(fournisseur);
            }

            br.close(); // Close the BufferedReader when done
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

