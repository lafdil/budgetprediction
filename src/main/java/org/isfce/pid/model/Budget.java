package org.isfce.pid.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TBUDGET")
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "ORDINAIRE_AMOUNT")
    private double ordinaireAmount;

    @Column(name = "EXTRAORDINAIRE_AMOUNT")
    private double extraordinaireAmount;

    @Column(name = "ANNEE")
    private int annee;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SITE_ID", nullable = false)
    private Site site; // Reference to Site

    // Constructor (if needed)

    // Getters and Setters (if needed)
}
