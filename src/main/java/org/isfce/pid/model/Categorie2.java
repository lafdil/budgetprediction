/*
 * package org.isfce.pid.model;
 * 
 * import java.util.Set;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.GeneratedValue; import
 * javax.persistence.GenerationType; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.ManyToOne; import
 * javax.persistence.OneToMany; import javax.validation.constraints.NotNull;
 * 
 * import lombok.AllArgsConstructor; import lombok.Data; import
 * lombok.NoArgsConstructor;
 * 
 * @Data
 * 
 * @NoArgsConstructor
 * 
 * @AllArgsConstructor
 * 
 * @Entity(name = "TCATEGORIE") public class Categorie {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private int id;
 * 
 * @Column(name = "NOMCATEGORIE", nullable = false) private String nomCategorie;
 * 
 * private String description;
 * 
 * @OneToMany(mappedBy = "categorie", cascade = CascadeType.ALL) private
 * Set<Produit> ProduitList;
 * 
 * @OneToMany(mappedBy = "categorie", cascade = CascadeType.ALL) private
 * Set<SubCategorie> subCategorieList;
 * 
 * @Override public String toString() { return String.valueOf(id); }
 * 
 * 
 * }
 */