package org.isfce.pid.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.isfce.pid.controller.dto.FournisseurDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TFOURNISSEUR")
public class Fournisseur {

    @Id
    private Integer id;

    @NotNull
    @Column(name = "NOM_FOURNISSEUR", length = 50, nullable = false)
    private String nomFournisseur;

    @OneToMany(mappedBy = "fournisseur")
    private List<Produit> produits;

    // Other attributes and methods as needed

    
    public FournisseurDto toDto() {
        FournisseurDto fournisseurDto = new FournisseurDto();
        fournisseurDto.setId(this.id);
        fournisseurDto.setNomFournisseur(this.nomFournisseur);
        // Assuming that you don't have any other fields in FournisseurDto

        return fournisseurDto;
    }
}
