package org.isfce.pid.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TLIGNECMD") // Set the name of the join table explicitly
public class LigneCmd {

	@EmbeddedId
	private LigneCmdPkId id;

	@ManyToOne

	@JoinColumn(name = "FKCOMMANDE", insertable = false, updatable = false)
	private Commande commande;

	@ManyToOne
	@JoinColumn(name = "FKPRODUIT", insertable = false, updatable = false)
	private Produit produit;

	private int quantite;

	private double prix;
	
    private String whatType = "-";

    private String details = "-";
    private String code;

}