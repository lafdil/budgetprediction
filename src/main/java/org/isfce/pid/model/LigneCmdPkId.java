package org.isfce.pid.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable

public class LigneCmdPkId implements Serializable {

	
	@Column(name = " FKCOMMANDE ")
	private int commande;

	@Column(name = " FKPRODUIT ")
	private int produit;
	
	  // Manually add the getId() method
    public int getId() {
        return this.commande; // You can choose to return either commande or produit based on your requirements
    }

}
