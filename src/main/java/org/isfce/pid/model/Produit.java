package org.isfce.pid.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.isfce.pid.controller.dto.ProduitDto;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TPRODUIT")
public class Produit {

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Column(name = "NOM_PRODUIT", length = 40, nullable = false)
	private String nomProduit;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CATEGORIE_ID", nullable = false)
	private Categorie categorie;


	@NotNull
	@Column(name = "PRIX_PAR_PIECE")
	private double prix;

	@NotNull
	@Column(name = "STOCK")
	private int stock;

	@NotNull
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Transient
	@Column(name = "IS_ORDINAIRE", nullable = false, columnDefinition = "boolean default false")
	private boolean isOrdinaire;


	@ManyToOne
	@JoinColumn(name = "FOURNISSEUR_ID")
	private Fournisseur fournisseur;

	// Modify the constructor to include the new attribute
	public Produit(Integer id, String nomProduit, Categorie categorie, double prix, int stock, String description,
			boolean isOrdinaire) {
		this.id = id;
		this.nomProduit = nomProduit;
		this.categorie = categorie;
		this.prix = prix;
		this.stock = stock;
		this.description = description;
		this.isOrdinaire = isOrdinaire;
	}
	public ProduitDto toDto() {
	    ProduitDto produitDto = new ProduitDto();
	    produitDto.setId(id); 
	    produitDto.setNomProduit(nomProduit);
	    produitDto.setPrix(prix);
	    produitDto.setStock(stock);
	    produitDto.setDescription(description);
	    produitDto.setOrdinaire(isOrdinaire);
	    
	    // Set the categorie attribute
	    produitDto.setCategorie(categorie);
	    
	    return produitDto;
	}


}
