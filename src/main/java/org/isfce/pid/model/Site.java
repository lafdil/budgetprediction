package org.isfce.pid.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.isfce.pid.controller.dto.SiteDto;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TSITE")
public class Site {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column(name = "NOM_SITE", length = 40, nullable = false)
	private String nomSite;

	@Column(name = "NOM_DEMANDEUR", length = 40)
	private String nomDemandeur;

	 @OneToOne(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	    private Budget budget; // Reference to Budget

	@Column(name = "ADRESSE", length = 100)
	private String adresse;

	@OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Commande> commands;

	public void addCommand(Commande command) {
		commands.add(command);
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	 public SiteDto toDto() {
	        SiteDto siteDto = new SiteDto();
	        siteDto.setId(this.id);
	        siteDto.setNomSite(this.nomSite);
	        siteDto.setNomDemandeur(this.nomDemandeur);
	        siteDto.setAdresse(this.adresse);
	        return siteDto;
	    }
	}

