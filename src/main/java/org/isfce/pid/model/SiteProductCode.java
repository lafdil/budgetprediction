package org.isfce.pid.model;

import javax.persistence.*;

@Entity
@Table(name = "SITE_PRODUCT_CODE")
public class SiteProductCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "SITE_ID")
    private Site site;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private Produit produit;

    private String code;

    // Getters and setters...
}
