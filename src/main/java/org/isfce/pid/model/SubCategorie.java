/*
 * package org.isfce.pid.model;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.GeneratedValue; import javax.persistence.GenerationType;
 * import javax.persistence.Id; import javax.persistence.JoinColumn; import
 * javax.persistence.ManyToOne; import javax.validation.constraints.NotNull;
 * 
 * import lombok.AllArgsConstructor; import lombok.Data; import
 * lombok.NoArgsConstructor;
 * 
 * 
 * @Data
 * 
 * @NoArgsConstructor
 * 
 * @AllArgsConstructor
 * 
 * @Entity(name = "TSUBCATEGORIE") public class SubCategorie {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private int id;
 * 
 * @Column(name = "NOM_SUBCATEGORIE", nullable = false) private String
 * nomSubcategorie;
 * 
 * @ManyToOne
 * 
 * @JoinColumn(name = "CATEGORIE_ID", nullable = false) private Categorie
 * categorie;
 * 
 * // ... Add other attributes and methods as needed
 * 
 * @Override public String toString() { return String.valueOf(id); } }
 * 
 */