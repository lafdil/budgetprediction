package org.isfce.pid.service;

import java.util.Optional;

import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.dao.ICategorieJpaDao;
import org.isfce.pid.model.Categorie;
import org.isfce.pid.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategorieServices {
	
	
	@Autowired
	private ICategorieJpaDao iCategorieJpaDao;
	
	
	public Optional<Categorie> fetchCategorieById(int categoryId) {
	    return Optional.of(iCategorieJpaDao.findById(categoryId));
	}

	 

//	public Categorie findOne(int id) {
//		return iCategorieJpaDao.findById(id).orElseThrow(() -> new NotExistException("categorie", id));
//	}
	
}
