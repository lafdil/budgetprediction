package org.isfce.pid.service;

import java.util.Optional;

import org.isfce.pid.dao.ICommandeJpaDao;
import org.isfce.pid.model.Commande;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommandeServices {

	@Autowired
	private ICommandeJpaDao iCommandeJpaDao;

	public Commande saveCommande(Commande commande) {

		Commande newCommande = new Commande();
		newCommande.setSite(commande.getSite());
		System.out.println(commande.getSite());

		//List<LigneCmd> lignesCmdList = new ArrayList<>();

//		  LigneCmdPkId ligneCmdPkId = new
//		  LigneCmdPkId(newCommande.getLignesCmd().get(0).getProduit().getId(),
//		 newCommande.getId()); l
		// ignesCmdList.add(ligneCmdPkId,
//		  newCommande.getLignesCmd().get(0).getQuantity());
//		  newCommande.setLignesCmd(newCommande.getLignesCmd());

		// Create a new LigneCmd
	//	LigneCmd ligneCmd = new LigneCmd();
		// Set the product for this ligneCmd
		// Set the commande for this ligneCmd
	//	ligneCmd.setCommande(newCommande);
		// hier where i have a mistake?????
		// Set the quantity for this ligneCmd
	
		/*
		 * ligneCmd.setQuantity(commande.getLignesCmd().get(0).getQuantity());
		 * ligneCmd.setProduit(commande.getLignesCmd().get(0).getProduit());
		 * 
		 * // Add the LigneCmd to the lignesCmdList lignesCmdList.add(ligneCmd);
		 * 
		 * newCommande.setLignesCmd(lignesCmdList);
		 */

		return iCommandeJpaDao.save(newCommande);

	}
	
	public Commande insert(Commande commande) {

		return iCommandeJpaDao.save(commande);
	}
	
	 public Optional < Commande > getCommandeById(int commandeId){ 
		 return   iCommandeJpaDao.findById(commandeId);
}}
