package org.isfce.pid.service;

import java.util.List;

import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.dao.IFournisseurJpaDao;
import org.isfce.pid.model.Fournisseur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FournisseurServices {
	
		
	@Autowired
	private IFournisseurJpaDao  iFournisseurJpaDao;

	public List<Fournisseur> getAllFournisseurs() {
		return iFournisseurJpaDao.findAll();

	}
	
	public Fournisseur findOne(int id) {
		return iFournisseurJpaDao.findById(id).orElseThrow(() -> new NotExistException("fournisseur", id));
	}

	public Fournisseur update(Fournisseur fournisseur) {
		// Check that the Fournisseur object is not null
		assert fournisseur != null : "le Fournisseur doit exister";
		// Save the updated Fournisseur to the database and return it
		return iFournisseurJpaDao.save(fournisseur);
	} 
	
	/*
	 * public boolean exists(int id) {
	 * 
	 * return iFournisseurJpaDao.existsById(id);
	 * 
	 * }
	 */
	public boolean existsById(int id) {
		return iFournisseurJpaDao.existsById(id);
	}
	
	/**
	 * Suppression d'un ingredient s'il existe Il ne faut pas qu'il possède des
	 * liens vers...
	 * 
	 * @param code
	 */
	public boolean delete(int id) {
		iFournisseurJpaDao.deleteById(id);
		return true;

	}
	
	/**
	 * Ajout d'un nouveau Fournisseur
	 * 
	 * @param f1
	 * @return
	 */
	public Fournisseur insert(Fournisseur f1) {
		return iFournisseurJpaDao.save(f1);
	}
	
	// count() is in crud repo, so we do not have to mention it in Icerticat repo
		public int getNumberOfFournisseurs() {

			return (int) (iFournisseurJpaDao.count());
		}
	
	
}
