package org.isfce.pid.service;

import java.util.Set;

import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.dao.ILigneCmdJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.isfce.pid.model.LigneCmd;
import org.isfce.pid.model.LigneCmdPkId;
import org.isfce.pid.model.Produit;

@Service
public class LigneCmdServices {
	
	
	@Autowired
	private ILigneCmdJpaDao iLigneCmdJpaDao;
	
	public void insert(Set<LigneCmd> lignesCommande) {

		iLigneCmdJpaDao.saveAll(lignesCommande);
	}

	public LigneCmd findOne(int commandeId, int produitId) {
	    LigneCmdPkId id = new LigneCmdPkId(commandeId, produitId);
	    return iLigneCmdJpaDao.findById(id)
	            .orElseThrow(() -> new NotExistException("LigneCmd.notExist"));
	}
	
	
}
