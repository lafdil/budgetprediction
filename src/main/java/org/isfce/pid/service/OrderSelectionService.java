package org.isfce.pid.service;
import java.util.List;

import org.isfce.pid.model.Produit;
import org.isfce.pid.model.Site;

public interface OrderSelectionService {
	
	  List<Site> getAllSites();
	    List<Produit> getAllProducts();
	    void selectSite(Site site);
	    void selectProduct(Produit product, int quantity);
	    void removeProduct(Produit product);
	    void clearSelection();
	    // Add any other methods as needed
	}

