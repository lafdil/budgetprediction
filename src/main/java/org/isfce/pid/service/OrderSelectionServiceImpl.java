package org.isfce.pid.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.isfce.pid.model.Produit;
import org.isfce.pid.model.Site;
import org.springframework.stereotype.Service;

@Service
public class OrderSelectionServiceImpl implements OrderSelectionService {

    private Site selectedSite;
    private Map<Produit, Integer> selectedProducts;

    public OrderSelectionServiceImpl() {
        this.selectedProducts = new HashMap<>();
    }

    @Override
    public List<Site> getAllSites() {
        // Implement logic to retrieve all sites from the database
        // You can use a SiteRepository or EntityManager to fetch the sites
        // Return the list of sites
    	return null;
    }

    @Override
    public List<Produit> getAllProducts() {
		
        // Implement logic to retrieve all products from the database
        // You can use a ProduitRepository or EntityManager to fetch the products
        // Return the list of products
    	return null;
    }

    @Override
    public void selectSite(Site site) {
        this.selectedSite = site;
    }

    @Override
    public void selectProduct(Produit product, int quantity) {
        selectedProducts.put(product, quantity);
    }

    @Override
    public void removeProduct(Produit product) {
        selectedProducts.remove(product);
    }

    @Override
    public void clearSelection() {
        selectedSite = null;
        selectedProducts.clear();
    }

    // Add implementations for any other methods in the interface if needed
}
