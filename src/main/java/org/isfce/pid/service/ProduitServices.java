package org.isfce.pid.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.dao.IProduitJpaDao;
import org.isfce.pid.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Transactional
@Service
public class ProduitServices {

	@Autowired
	private IProduitJpaDao iProduitJpaDao;

	public List<Produit> getAllProduits() {
		return iProduitJpaDao.findAll();

	}

	// count() is in crud repo, so we do not have to mention it in Icerticat repo
	public int getNumberOfProduits() {

		return (int) (iProduitJpaDao.count());
	}

	/**
	 * Vérifie si un produit existe
	 * 
	 * @param id
	 * @return
	 */

	/*
	 * public boolean exists(int id) {
	 * 
	 * return iProduitJpaDao.existsById(id);
	 * 
	 * }
	 */
	public boolean existsById(int id) {
		return iProduitJpaDao.existsById(id);
	}

	public Produit update(Produit produit) {
		// Check that the produit object is not null
		assert produit != null : "le produit doit exister";
		// Save the updated produit to the database and return it
		return iProduitJpaDao.save(produit);
	} 
	
	/**
	 * Suppression d'un ingredient s'il existe Il ne faut pas qu'il possède des
	 * liens vers...
	 * 
	 * @param code
	 */
	public boolean delete(int id) {
		iProduitJpaDao.deleteById(id);
		return true;

	}
	

	/**
	 * Ajout d'un nouveau Produit
	 * 
	 * @param p1
	 * @return
	 */
	public Produit insert(Produit p1) {
		return iProduitJpaDao.save(p1);
	}

	/**
	 * retourne un cours à partir de son id
	 * 
	 * @param id
	 * @return un Optional de Produit
	 */
	/*
	 * public Optional<Produit> findOne(int id) { return
	 * iProduitJpaDao.findById(id); }
	 */

	public Produit findOne(int id) {
		return iProduitJpaDao.findById(id).orElseThrow(() -> new NotExistException("produit", id));
	}

	public Produit findByNomProduitIgnoreCase(String nomProduit) {
		return iProduitJpaDao.findByNomProduitIgnoreCase(nomProduit);
	}

	public List<Produit> findAll() {
		return iProduitJpaDao.findAll();
	}
}
