package org.isfce.pid.service;

import java.util.Optional;

import org.isfce.pid.dao.ISiteProductCodeRepositoryJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiteProductCodeServices {

    @Autowired
    private ISiteProductCodeRepositoryJpaDao iSiteProductCodeRepositoryJpaDao;

    public Optional<String> getCodeBySiteAndProduct(int siteId, int productId) {
        return iSiteProductCodeRepositoryJpaDao.findCodeBySiteAndProduct(siteId, productId);
    }
}
