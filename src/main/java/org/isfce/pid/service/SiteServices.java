package org.isfce.pid.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.dao.ISiteJpaDao;
import org.isfce.pid.model.Produit;
import org.isfce.pid.model.Site;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiteServices {

	@Autowired
	private ISiteJpaDao iSiteJpaDao;

	public List<Site> getAllSites() {
		return iSiteJpaDao.findAll();

	}

	public List<Object[]> findCommandesWithTotalPrice(Integer siteId) {
		return iSiteJpaDao.findCommandesWithTotalPrice(siteId);
	}

	public List<Object[]> findCommandesBySiteAndYear(Integer siteId, Integer year) {
		return iSiteJpaDao.findCommandesBySiteAndYear(siteId, year);
	}

	public Site getSiteById(Integer siteId) {
		Optional<Site> optionalSite = iSiteJpaDao.findById(siteId);
		return optionalSite.orElse(null);
	}

	public Site findOne(int id) {
		return iSiteJpaDao.findById(id).orElseThrow(() -> new NotExistException("site", id));
	}

	public int getNumberOfSites() {

		return (int) (iSiteJpaDao.count());
	}

	public boolean existsById(int id) {
		return iSiteJpaDao.existsById(id);
	}

	/**
	 * Ajout d'un nouveau Site
	 * 
	 * @param s1
	 * @return
	 */
	public Site insert(Site s1) {
		return iSiteJpaDao.save(s1);
	}

	public Site update(Site site) {
		// Check that the produit object is not null
		assert site != null : "le Site doit exister";
		// Save the updated Site to the database and return it
		return iSiteJpaDao.save(site);
	}

	/**
	 * Suppression d'un Site s'il existe Il ne faut pas qu'il possède des
	 * liens vers...
	 * 
	 * @param code
	 */
	public boolean delete(int id) {
		iSiteJpaDao.deleteById(id);
		return true;

	}
	
	
}
