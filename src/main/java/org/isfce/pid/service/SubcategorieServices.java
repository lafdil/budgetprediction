/*
 * package org.isfce.pid.service;
 * 
 * import org.isfce.pid.controller.exceptions.NotExistException; import
 * org.isfce.pid.dao.ICategorieJpaDao; import org.isfce.pid.model.Site; import
 * org.isfce.pid.model.Categorie; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.data.jpa.repository.JpaRepository; import
 * org.springframework.stereotype.Service;
 * 
 * @Service public class SubcategorieServices {
 * 
 * @Autowired private ICategorieJpaDao iSubcategorieJpaDao;
 * 
 * 
 * public Categorie findByNomSubcategorieIgnoreCase(String nomSubcategorie) {
 * return iSubcategorieJpaDao.findByNomSubcategorieIgnoreCase(nomSubcategorie);
 * }
 * 
 * 
 * public Categorie getById(int subcategorieId) { return
 * iSubcategorieJpaDao.findById(subcategorieId) .orElseThrow(() -> new
 * NotExistException("not found", subcategorieId)); } }
 */