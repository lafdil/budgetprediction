MERGE INTO TUSER VALUES
('admin','$2a$10$BR98DmjHecYVdqtNz3UGcuyFop/ed0KfxOEAp1bdVXh8eF2iAfOz.','0' ),
('vo', '$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq','1'),
('wa','$2a$10$ZZ4U5deuZBQqp/BNmKrG1e6OqiRwmaAfu.aaQfgBI9ehImH8AYZKC','1'),
('sh','$2a$10$PdX74d5AzJN/OVU3bP1XbO3fLSfr6x3oKC81rbiUAlSEE6DcID986',1),
('et1','$2a$10$N8pzEKs1350SPT1vpomUo.zMEV1IR/LjXgOrBJ.QaeddZtKLVxR4q',2),
('et2','$2a$10$.Tlzs3a.2PEGK1gckhgaJuz4SWW2J1lWBf.4E3rOleziGDpExtp1W',2),
('et3','$2a$10$eeilUxVqPB8dXMsnzxiIp.OlOyD1isPtICWaRRL5VFofqcWcSURk2',2),
('et4','$2a$10$35yXfuSOyZkLLXFKXQbDLuzvdepJA2cFwxqu.VOsDuuAgf5ujVOuu',2),
('et5','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',2),
('sec1','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3),
('sec2','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3);


MERGE INTO TCATEGORIE (ID, NOMCATEGORIE, description) VALUES
(1, 'Applications', 'micro'),
(2, 'Consultance', 'micro'),
(3, 'Ecrans', 'IT'),
(4, 'Matériels', 'IT'),
(5, 'Petits Matériels', 'micro'),
(6, 'réseau', 'IT'),
(7, 'Téléphonie', 'IT');




MERGE INTO TPRODUIT (id, NOM_PRODUIT, CATEGORIE_ID, PRIX_PAR_PIECE, STOCK, DESCRIPTION) VALUES
(1, 'pc_hp', 4, 10, 60, 'Ordinateur'),
(2, 'Lignes internet', 1, 15, 100, 'Location Irisnet');




MERGE INTO TSITE (ID, NOM_DEMANDEUR, NOM_SITE, ADRESSE ) VALUES
(1,'JDLC', 'JDLC', 'Rue Gérard 140, 1040 Etterbeek'),
(2,'ISFCE', 'ISFCE', 'Rue Joseph Buedts 14, 1040 Etterbeek');


MERGE INTO SITE_PRODUCT_CODE (ID, SITE_ID, PRODUCT_ID, CODE) VALUES
(1, 1, 1, '139/123-11'),
(2, 2, 1, '139/123-12'),
(3, 1, 2, '139/123-13'),
(4, 2, 2, '139/123-14');







 


