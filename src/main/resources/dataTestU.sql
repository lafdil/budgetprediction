MERGE into TCOURS(CODE,NB_PERIODES,NOM) values 
('IGEB',60,'Gestion de base de données'),
('IPID',60,'Projet de Développement'),
('IVTE',40,'Veille technologique'),
('ISO2',60,'Structure des ordinateurs'),
('IPAP',120,'Principes d''algorithmique et de Programmation'),
('XTAB',80,'Base de données et Tableur'),
('SO2',60,'Architecture des ordinateurs');

delete from TSECTION;
insert  into TSECTION(FKCOURS,SECTION) values ('XTAB','Comptabilité');
insert  into TSECTION(FKCOURS,SECTION) values ('XTAB','Secrétariat');
insert  into TSECTION(FKCOURS,SECTION) values('IPID','Informatique');
insert  into TSECTION(FKCOURS,SECTION) values('IGEB','Informatique');
insert  into TSECTION(FKCOURS,SECTION) values('ISO2','Informatique');
insert  into TSECTION(FKCOURS,SECTION) values('IVTE','Informatique');
insert  into TSECTION(FKCOURS,SECTION) values('IPAP','Informatique');
insert  into TSECTION(FKCOURS,SECTION) values('ISO2','Informatique');

 MERGE INTO TUSER VALUES
('admin','$2a$10$BR98DmjHecYVdqtNz3UGcuyFop/ed0KfxOEAp1bdVXh8eF2iAfOz.','0' ),
('vo', '$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq','1'),
('wa','$2a$10$ZZ4U5deuZBQqp/BNmKrG1e6OqiRwmaAfu.aaQfgBI9ehImH8AYZKC','1'),
('sh','$2a$10$PdX74d5AzJN/OVU3bP1XbO3fLSfr6x3oKC81rbiUAlSEE6DcID986',1),
('et1','$2a$10$N8pzEKs1350SPT1vpomUo.zMEV1IR/LjXgOrBJ.QaeddZtKLVxR4q',2),
('et2','$2a$10$.Tlzs3a.2PEGK1gckhgaJuz4SWW2J1lWBf.4E3rOleziGDpExtp1W',2),
('et3','$2a$10$eeilUxVqPB8dXMsnzxiIp.OlOyD1isPtICWaRRL5VFofqcWcSURk2',2),
('et4','$2a$10$35yXfuSOyZkLLXFKXQbDLuzvdepJA2cFwxqu.VOsDuuAgf5ujVOuu',2),
('et5','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',2),
('sec1','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3),
('sec2','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3);

DELETE FROM TPROFESSEUR;
INSERT INTO TPROFESSEUR(ID,NOM,PRENOM,EMAIL,FKUSER) VALUES
(1,'Van Oudenhove','Didier','vo@isfce.be','vo');
INSERT INTO TPROFESSEUR(ID,NOM,PRENOM,EMAIL,FKUSER) VALUES
(2,'Wafflard','Alain','waff@isfce.be','wa');
INSERT INTO TPROFESSEUR(ID,NOM,PRENOM,EMAIL,FKUSER) VALUES
(3,'Huguier','Stephane','sh@isfce.be','sh');
alter table TPROFESSEUR ALTER COLUMN ID RESTART WITH 4;


MERGE INTO TETUDIANT (ID,NOM,PRENOM,EMAIL,FKUSER,TEL) VALUES
(1 , 'Nom_ET1', 'Prenom_ET1' , 'et1@isfce.be', 'et1', '02/800.00.01'),
(2 , 'Nom_ET2', 'Prenom_ET2', 'et2@isfce.be', 'et2', '02/800.00.02'),
(3 , 'Nom_ET3', 'Prenom_ET3', 'et3@isfce.be', 'et3', '02/800.00.03'),
(4 , 'Nom_ET4', 'Prenom_ET4', 'et4@isfce.be', 'et4', '02/800.00.04');
--Réinitialise le compteur identity 
alter table TETUDIANT ALTER COLUMN ID RESTART WITH (select max(id)+1 from TETUDIANT);

MERGE into TMODULE(CODE,DATE_DEBUT,DATE_FIN,MOMENT,FKCOURS,FKPROFESSEUR) values 
('IGEB-1-A','2023-02-03','2023-06-30','1','IGEB','1'),
('IGEB-2-A','2023-02-03','2023-06-30','2','IGEB','1'),
('IVTE-1-A','2022-11-23','2023-01-28','2','IVTE','1'),
('IPID-1-A','2022-09-10','2023-01-28','2','IPID','1'),
('IPAP-1-A','2023-01-31','2023-06-26','1','IPAP','1'),
('IPAP-2-A','2022-01-31','2023-06-26','2','IPAP','1'),
('ISO2-1-A','2022-09-10','2023-01-28','1','ISO2','3'),
('ISO2-2-A','2022-09-10','2023-01-28','2','ISO2','3');


MERGE INTO TINSCRIPTION( FKMODULE, FKETUDIANT ) VALUES
('IPID-1-A', 1),
('IGEB-1-A', 1),
('IGEB-2-A', 1),
('IGEB-1-A', 3),
('IGEB-1-A', 4),
('IPID-1-A',2),
('IGEB-1-A',2),
('IPID-1-A',3),
('IVTE-1-A',1),
('IVTE-1-A',3),
('ISO2-1-A',3),
('ISO2-1-A',4),
('IPAP-1-A',3),
('IPAP-2-A',4),
('IGEB-2-A', 1),
('IGEB-2-A',2);






MERGE INTO TSEANCE(code,duree,seance_date,FKMODULE,is_Cloture) VALUES
	('IGEB-1-A-s1',3,'2023-03-20','IGEB-1-A',false),
	('IGEB-1-A-s2',3,'2023-04-25','IGEB-1-A',true ),
	('IGEB-1-A-s3',2,'2023-03-10','IGEB-1-A',false ),
    	('IGEB-2-A-s1',4,'2023-04-25','IGEB-2-A',false ),
	('IGEB-2-A-s2',1,'2023-04-12','IGEB-2-A',true ),
	('IGEB-2-A-s3',1,'2023-04-14','IGEB-2-A',false );



MERGE INTO TSECRETAIRE (ID,NOM,PRENOM,EMAIL,FKUSER,TEL) VALUES
(1 , 'Nom_SEC1', 'Prenom_SEC1' , 'SEC1@isfce.be', 'sec1', '02/800.00.01'),
(2 , 'Nom_SEC2', 'Prenom_SEC1' , 'SEC2@isfce.be', 'sec2', '02/800.00.02');


MERGE INTO TCERTIFICAT( id, dateDebut, dateFin, ETUDIANT_ID, nom_Medecin) VALUES
    (1,'2023-02-03','2023-02-25', 1, 'Appelboom' ),
    (2,'2023-03-03','2023-4-5', 2,'Jean-François' ),
    (3,'2023-04-03','2023-05-25', 3,'André' );
--Réinitialise le compteur identity 
alter table TCERTIFICAT ALTER COLUMN ID RESTART WITH (select max(id)+1 from TCERTIFICAT);





 


